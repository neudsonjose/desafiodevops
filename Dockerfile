FROM python:3

WORKDIR /usr/src/app/

RUN apt update && apt install -y git

COPY * ./

RUN export S3_URL=S3_URL S3_ACCESS_KEY=S3_ACCESS_KEY S3_SECRET_KEY=S3_SECRET_KEY

RUN apt install -y python3 python3-pip

RUN pip3 install pipenv

RUN pipenv --python 3 install --system --deploy

RUN chmod +x start.sh

EXPOSE  5000

CMD ["./start.sh"]
