# Desafio devops

## Tecnologias utilizadas
`Docker, Docker-Composer, Ansible e GitLabCI`

Para a instalação do ambiente foi utilizado o `Ansible`, para inciar a aplicação foi configurado criado um `Dockerfile` e para gerenciamento do container utilizou-se o `Docker Composer`, além desses foi utilizado o `GitlabCI` como forma de automatizar os deploys da aplicação.

## Execução

Todos os arquivos necessários para replicar esse cenário estão disponíveis nesse repositório.

Primeiro se faz necessário inicializar as variáveis de ambiente no seguinte arquivo.

```bash
ansible/vars/default.yml
```
As variáveis são as seguintes:
```bash
S3_URL: URL do servidor minio
S3_ACCESS_KEY: usuario de acesso do servidor do min io
S3_SECRET_KEY: senha de acesso do servidor minIO
GIT_REPOSITORY: repositório do git de onde o projeto deve ser buscado
```
Este arquivo já se encontra preenchido com as variávies padrão, para que toda a aplicação possa ser utilizada em uma única instância, porém pode ser utilizado um servidor minIO fora da mesma.

Para inciar a instalação do ambiente de produção, basta executar um git clone do repositório onde está a aplicação acessar o diretório ansible

Editar o arquivo hosts-comp.yml informar as seguintes informações.
```bash
ansible_host: IP do host onde será configurado o ambiente
ansible_ssh_port: Porta SSH
ansible_user: usuário padrão para logar no host
ansible_become: yes Para que o ansible possa se tornar usuário root ou outro usuário com permissão de leitura e escrita no diretório /etc/
ansible_become_user: root informar o usuário nesse exemplo o root 
ansible_become_method: sudo e qual método o ansible irá utilizar para mudar de usuário
como a distribuição de destino é ubuntu utilizamos o sudo.
```

OBS: por questões de segurança não estamos passando uma senha no arquivo do ansible, quem executar o processo, pode gerar uma chave RSA, e adicionar sua chave pública dentro do authorized_keys assim sem a necessidade de utilizar a senha para se conectar no host, ou então se possuir uma chave do formato .pem utilizar a seguinte opção no arquivo do hosts-comp.yml:
```bash 
ansible_ssh_private_key_file: Informar o caminho da chave de acesso
```
Com essas variáveis ajustadas basta executar o seguinte comando.
```bash
ansible-playbook -i hosts-comp.yml clients.yml
```
OBS: Quem executar deve ter o ansible instalado.

Após toda a execução do playbook a aplicação já estará disponível e funcional.

### GitlabCI
Para automação dos deploys da aplicação foi utilizado o GitlabCI, entretanto, algumas configurações extras devem ser feitas criando as seguintes variáveis

$SSH_PRIVATE_KEY 
$USR_DESAFIODEVOPS
$IP_DESAFIODEVOPS

Para tal basta acessar o seu projeto e acessar o menu: settings -> CI/CD -> variables
e adicionar as variáveis

Para a chave SSH basta seguir os passos encontrados no tópico `Add SSH key` nesse proprio link do `Gitlab`:
`https://docs.gitlab.com/ee/ci/examples/laravel_with_gitlab_and_envoy/index.html`
Todo o procedimento ignorando apenas a parte final do git clone.

O arquivo gitlab-ci.yml foi configurado para rodar a pipeline, apenas no momento que houver um commit na branch master.

A pipeline tem dois estágios 1 de teste e  outro de build, no de teste iniciamos a aplicação para validar se com as auterações do código algum erro foi gerado.

Para os testes serem mais efetivos basta apontar a aplicação para um servidor minio válido e adicionar o comando para enviar um arquivo, se tudo correr bem ele passará para o deploy.

No estágio de deploy, ele acessa o host remoto e executa os comandos necessários para carregar os novos códigos e buildar novamente os containers 

**PS:** Infelizmente durante os testes de deploy foram encontrados problemas relacionados a esse issue do pipenv: `https://github.com/pypa/pipenv/issues/1356`
Que travou a instância e impediu que fossem realizados novos testes para validar completamente o deploy continuo da aplicação.